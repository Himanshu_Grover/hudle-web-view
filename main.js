var startDate = moment();
var endDate = startDate.clone().add(3, 'day');

console.log('startDate: ', startDate.format('YYYY-MM-DD'));
console.log('endDate: ', endDate.format('YYYY-MM-DD'));

var localArrayGlobal = [];
localArrayGlobal[0] = JSON.parse('{"id":"e7fc7429-8122-4dc8-98a2-234629b9df40","facility_id":1,"price":"500.00","start_time":"2018-01-29 11:15:00","end_time":"2018-01-29 12:00:00","available_count":1,"is_available":true,"created_at":"2018-01-02 16:24:37","updated_at":"2018-01-02 16:24:37"}');
localArrayGlobal[1] = JSON.parse('{"id":"777cba36-0d38-4a87-95fc-e9031f741890","facility_id":1,"price":"500.00","start_time":"2018-01-28 10:30:00","end_time":"2018-01-28 11:15:00","available_count":1,"is_available":true,"created_at":"2018-01-02 16:24:37","updated_at":"2018-01-02 16:24:37"}');
localArrayGlobal[2] = JSON.parse('{"id":"47752f64-a37c-4cb8-b9bc-b39645106e38","facility_id":1,"price":"500.00","start_time":"2018-01-29 12:00:00","end_time":"2018-01-29 12:45:00","available_count":1,"is_available":true,"created_at":"2018-01-02 16:24:37","updated_at":"2018-01-02 16:24:37"}');

console.log("localArrayGlobal", JSON.parse(JSON.stringify(localArrayGlobal)))

// Construction of Grids
var makeGrid = function(startDate, endDate, localArray) {
    $.ajax({ 
        type: 'GET', 
        url: 'http://hudle-staging.squareboat.com/api/v1/venues/431728fa-fb7d-45ca-8be1-3651cb0cf791/facilities/ac265925-69a4-495f-8309-ff1a6cbe1c04/slots?start_date=2018-01-03&end_date=2018-01-07&grid=1', 
        headers: { 'Api-Secret': 'hudle-api1798' },
        data: { start_date: startDate, end_date: endDate }, 
        dataType: 'json',
        success: function (response) {
            
            var selectedSlots = {};
            var slotTimingTrigger = true;
            var jsonData = response.data.slot_data;
            var jsonDataSlotTimings = response.data.slot_timings;
            var jsonDataLength = jsonData.length;
            var jsonDataSlotTimingsLength = jsonDataSlotTimings.length;
            var currentDateTime = moment().format('DD-MM-YYYY HH:mm:ss');
            var currentDate = moment().format('DDMMYYYY');
            var currentTime = moment().format('HHmm');
            console.log('jsonData', response);
            $('#gridLayoutInner').html('');
            for (var i = 0; i <= jsonDataLength; i++) {
                $('#gridLayoutInner').append('<div id="gridLayoutCol' + i + '" class="grid-col main-col">');
                if(slotTimingTrigger) {
                    var count = 1;
                    console.log('jsonDataSlotTimingsLength', jsonDataSlotTimingsLength);
                    for (var s = 1; s <= jsonDataSlotTimingsLength; s++) {
                        if(count == 1) {
                            $('#gridLayoutCol' + i).append('<div id="gridLayoutColGrid' + i + s + '" class="grid row-header"></div>');
                        }
                        $('#gridLayoutCol' + i).append('<div id="gridLayoutColGrid' + i + s + '" class="grid row-header"><div class="from">' + moment(jsonDataSlotTimings[s - 1].from, "h:mm a").format("hh:mm a") + '</div><div class="to">' + moment(jsonDataSlotTimings[s - 1].to, "h:mm a").format("hh:mm a") + '</div>');
                        count++;
                    }
                    slotTimingTrigger = false;
                } else {
                    var jsonDataSlots = jsonData[i - 1].slots;
                    var jsonDataSlotsLength = jsonDataSlots.length;
                    var count = 1;
                    for (var j = 1; j <= jsonDataSlotsLength; j++) {
                        var availability = 'NA';
                        var na = false;
                        var className = '';
                        if (moment(jsonDataSlots[j - 1].start_time).isBefore()) { na = true; }
                        if (na || !jsonDataSlots[j - 1].is_available) { className = 'not-available'; } else if (jsonDataSlots[j - 1].available_count == 0) { className = 'sold-out'; }
                        if(jsonDataSlots[j - 1].is_available) { availability = 'available' }
                        if (count == 1) {
                            $('#gridLayoutCol' + i).append('<div id="gridLayoutColGrid' + i + j + '" class="grid"><span class="date">' + moment(jsonData[i - 1].date).format("DD") + '</span><div class="day">' + moment(jsonData[i - 1].date).format("ddd") + '</div></div>');
                        }
                        $('#gridLayoutCol' + i).append('<div id="gridLayoutColGrid' + i + j + '" class="grid slot ' + className + '" data-index="' + (j - 1) + i + '" data-id="' + jsonDataSlots[j - 1].id + '" data-object=\'' + JSON.stringify(jsonDataSlots[j - 1]) + '\'"><div class="slot-content"><div class="price"><div>₹' + parseInt(jsonDataSlots[j - 1].price) + '</div></div><div class="slots-left">' + jsonDataSlots[j - 1].available_count + ' left</div><small class="availability">(' + availability + ')</small></div></div>');
                        count++;
                    }
                    $('#gridLayoutInner').append('</div>');
                }
            }

            console.log("localArray", localArray);
            localArray = JSON.parse(localArray);
            console.log("localArray", localArray);
            selectedSlots = localArray;
            // selectedSlots = localArray.map(function(elem, index) {
            //     console.log('elem:', JSON.parse(elem).id);
            //     return JSON.parse(elem);
            // });
            console.log('selectedSlots', selectedSlots);

            // Assigning pre-selected values
            $('.grid.slot').each(function(index, el) {

                var slotsToBeDeleted = [];

                for (var la = 0; la < selectedSlots.length; la++) {
                    var laDataObject = $(this).attr('data-object');
                    var laDataKey = $(this).attr('data-index');
                    var la_id = selectedSlots[la].id;
                    var la_obj = selectedSlots[la];
                    if($(this).attr('data-id') == la_id) {
                        if(!$(el).hasClass('sold-out') && !$(el).hasClass('not-available')) {
                            $(el).addClass('selected');
                            //selectedSlots[laDataKey] = la_obj;
                        } else {
                            slotsToBeDeleted.push(la);
                        }
                        // if($(this).hasClass('selected')) {
                        //     selectedSlots[laDataKey] = JSON.parse(laDataObject);
                        // } else {
                        //     delete selectedSlots[laDataKey];
                        // }
                    
                    }
                }

                //delete slots found
                for (var ds = 0; ds < slotsToBeDeleted.length; ds++) {
                    console.log("to be deleted", selectedSlots[slotsToBeDeleted[ds]]);
                    delete selectedSlots[slotsToBeDeleted[ds]];
                    selectedSlots = selectedSlots.filter(function(a){return typeof a !== 'undefined';}) //again put the data as delete doesn't properly delete the items
                }

                slotsToBeDeleted = [];

            });

            console.log('ARRAY: ', selectedSlots);
            console.log('PREFERRED: ', Object.values(selectedSlots));

            // Selection Logic
            $('.grid.slot').on('click', function(e) {
                var dataObject = $(this).attr('data-object');
                var dataKey = $(this).attr('data-index');
                var dataObjectId = JSON.parse(dataObject).id;
                console.log('dataObject', dataObject, dataKey);
                $(this).toggleClass('selected');
                if($(this).hasClass('selected')) {
                    selectedSlots.push(JSON.parse(dataObject));
                    //selectedSlots[dataKey] = JSON.parse(dataObject);
                } else {
                    for (var ds = 0; ds < selectedSlots.length; ds++) {
                        if(selectedSlots[ds].id == dataObjectId) {
                            console.log("to be deleted", selectedSlots[ds]);
                            delete selectedSlots[ds];
                            selectedSlots = selectedSlots.filter(function(a){return typeof a !== 'undefined';}) //again put the data as delete doesn't properly delete the items
                        }
                    }
                }

                localArrayGlobal = []; //empty array
                localArrayGlobal = JSON.parse(JSON.stringify(Object.values(selectedSlots))); //assign all the selected slots to local array

                console.log('ARRAY: ', selectedSlots);
                console.log('PREFERRED: ', Object.values(selectedSlots));
                console.log('Local Array Global: ', localArrayGlobal);
            });
        }
    });
}

// Navigation Logic (Previous)
$('.navigation-controls .control.previous').on('click', function(event) {
    console.log('Previous Clicked!');
    endDate = startDate.clone().add(-1, 'days');
    startDate = startDate.clone().add(-4,'days');
    makeGrid(startDate.format('YYYY-MM-DD'), endDate.format('YYYY-MM-DD'), JSON.stringify(localArrayGlobal));
});

// Navigation Logic (Next)
$('.navigation-controls .control.next').on('click', function(event) {
    console.log('Next Clicked!');
    startDate = endDate.clone().add(1, 'days');
    endDate = startDate.clone().add(3,'days');
    makeGrid(startDate.format('YYYY-MM-DD'), endDate.format('YYYY-MM-DD'), JSON.stringify(localArrayGlobal));
});

makeGrid(startDate.format('YYYY-MM-DD'), endDate.format('YYYY-MM-DD'), JSON.stringify(localArrayGlobal));